﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3
{
    internal class PartTimeStudent : Student
    {
        string slot, courseCode;
        //public void GetPartTimeStudent()
        //{
        public override void GetDetails()
        { 
            base.GetDetails();
            Console.WriteLine("Enter slot");
            slot = Console.ReadLine();
            Console.WriteLine("ENter courseCode");
            courseCode = Console.ReadLine();
        }
        //public void DisplayPartTimeStudent()
        public override void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine("SLot is " + slot);
            Console.WriteLine("courseCode is " + courseCode);
        }
    }
}
